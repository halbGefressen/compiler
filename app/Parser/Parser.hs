{-# LANGUAGE FlexibleContexts #-}
module Parser.Parser (kek) where
import qualified Text.Parsec as Parsec
import Text.Parsec.Char (char)
import Control.Applicative
import qualified Parser.AST as AST
import Data.Maybe (fromJust)
kek = "kek"

uncurry3 f (a, b, c) = f a b c

parse rule text = Parsec.parse rule "(source)" text


type SimpleParser u = Parsec.Parsec String u Char




expr :: Parsec.Parsec String u AST.Expression -- yeah this is big brain time
expr = foldr template expr3 [cmp, op1, op2]
  where template ops expr = foldl (uncurry . AST.Combined) <$> expr <*> (Parsec.many ((,) <$> Parsec.choice parsers <*> expr))
                where parsers = op_parser <$> ops; op_parser (sym, val) = val <$ Parsec.string sym
        expr3 = Parsec.choice [(fmap AST.Val value), call, (fmap AST.Ident ident), (bl *> Parsec.spaces *> expr <* Parsec.spaces <* br)]
        cmp = [("<"  , AST.Lt),
               ("<=" , AST.Le),
               ("==" , AST.Eq),
               (">=" , AST.Ge),
               (">"  , AST.Gt),
               ("!=" , AST.Ne)]
        op1 = [("+"  , AST.Plus),
               ("-"  , AST.Minus)]
        op2 = [("*"  , AST.Star),
               ("/"  , AST.Slash),
               ("%"  , AST.Percent)]


call :: Parsec.Parsec String u AST.Expression
call = AST.Call <$> ident <*> args

args = do
  char '('
  Parsec.spaces
  ex <- expr
  Parsec.spaces
  exprs <- Parsec.many $ do
    comma
    Parsec.spaces
    ex <- expr
    Parsec.spaces
    return ex
  br
  return $ ex : exprs

ident = (:) <$> letter <*> (many $ letter <|> number)
value = do
  num <- Parsec.many1 number
  return $ AST.Num ((read num) :: Integer)

bl :: SimpleParser u
bl = char '('

br :: SimpleParser u
br = char ')'

cbl :: SimpleParser u
cbl = char '{'

cbr :: SimpleParser u
cbr = char '}'

assign :: SimpleParser u
assign = char '='

semic :: SimpleParser u
semic = char ';'

number = Parsec.digit

letter :: SimpleParser u
letter = Parsec.letter

ws :: SimpleParser u
ws = Parsec.space

comma :: SimpleParser u
comma = char ','
