module Parser.AST where

type Program = [Function]

data Function = Function { fn_ident :: Identifier, fn_args :: [Identifier], fn_stmt :: [Statement] }

data Statement =
  More [Statement] |
  If { stmt_expr :: Expression,
       stmtTt :: Statement,
       stmtFf :: Statement } |
  While { stmt_expr :: Expression,
          stmt :: Statement } |
  Do { stmt :: Statement, stmt_expr :: Expression } |
  Loop Statement |
  Break |
  Return |
  Assign { stmt_ident :: Identifier, stmt_expr :: Expression } |
  Declare { stmt_ident :: Identifier, assign :: Maybe Expression } |
  Standalone Expression |
  Empty


data Expression =
  Val Value |
  Ident Identifier |
  Call { call_ident :: Identifier, args :: Arguments } |
  Combined { expr1 :: Expression, op :: Operator, expr2 :: Expression }

data Operator = Gt | Ge | Eq | Le | Lt | Ne | Plus | Minus | Star | Slash | Percent

type Arguments = [Expression]

type Identifier = String
data Value = Num Integer | Tt | Ff

to_op :: Char -> Operator
to_op '+' = Plus
to_op '-' = Minus
to_op '*' = Star
to_op '/' = Slash
to_op '%' = Percent
